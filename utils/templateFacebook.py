def postbackButton(data):
    return {
        "recipient": {"id": data["recipientID"]},
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type":"button",
                    "text": "Try the postback button!",
                    "buttons": data["buttons"]
                }
            }
        }
    }