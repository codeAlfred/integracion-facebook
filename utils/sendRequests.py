import requests
import json

from utils.environment import Environment as Env
from utils.logger import LogError

class SendRequests(Env):
    def __init__(self):
        self.URL_API = None
        self.params = None
        self.headers = None

    def get(self, service, endpoint, data = None):
        self.optionConfig(service)

        response = requests.get(
            f'{self.URL_API}/{endpoint}',
            params=self.params,
            headers=self.headers,
            data=json.dumps(data)
        )

        if response.status_code != 200:
            LogError('Clase SendRequest', response.status_code)
            LogError('Clase SendRequest', response.text)
        else:
            return response.text

    def delete(self, service, endpoint, data = None):
        self.optionConfig(service)

        response = requests.delete(
            f'{self.URL_API}/{endpoint}',
            params=self.params,
            headers=self.headers,
            data=json.dumps(data)
        )

        if response.status_code != 200:
            LogError('Clase SendRequest', response.status_code)
            LogError('Clase SendRequest', response.text)
        else:
            return response.text

    def post(self, service, endpoint, data = None):
        self.optionConfig(service)

        print(f'{self.URL_API}/{endpoint}')

        response = requests.post(
            f'{self.URL_API}/{endpoint}',
            params=self.params,
            headers=self.headers,
            data=json.dumps(data)
        )

        if response.status_code != 200:
            LogError('Clase SendRequest', response.status_code)
            LogError('Clase SendRequest', response.text)
        else:
            return response.text

    def optionConfig(self, service):
        if service.upper() == 'FACEBOOK':
            self.configFacebook()

        if service.upper() == 'FOOTBALL':
            self.configFootBall()

    def configFacebook(self):
        # Almacenamos las variables env de conexión a Facebook
        envFacebook = self.facebook()
        ACCESS_TOKEN = envFacebook['ACCESS_TOKEN']
        self.URL_API = envFacebook['URL_API']

        # Configuramos los parametros generales para el envió
        # de información hacía Facebook
        self.params = { "access_token": ACCESS_TOKEN }
        self.headers = { "Content-Type": "application/json" }

    def configFootBall(self):
        # Almacenamos las variables env de conexión a Facebook
        envFootball = self.footBall()
        self.URL_API = envFootball['URL_API']

        # Configuramos los parametros generales para el envió
        # de información hacía Football
        self. headers = {
            'x-rapidapi-host': envFootball['X-RAPIDAPI-HOST'],
            'x-rapidapi-key': envFootball['X-RAPIDAPI-KEY']
        }
