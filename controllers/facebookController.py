import json
from flask import request

from utils.sendRequests import SendRequests
from utils.logger import LogInfo
from utils.templateFacebook import postbackButton

class FacebookController(SendRequests):

    def validate(self):
        # Obtener las variables de entorno para facebook.
        envFacebook = self.facebook()

        # Almacenamos los valores enviados como parametros en el request
        valueHubMode = request.args.get("hub.mode")
        valueHubChallenge = request.args.get("hub.challenge")
        valueHubToken = request.args.get("hub.verify_token")

        if valueHubMode == "subscribe" and valueHubChallenge:
            # Validar que el Token enviando por facebook sea idéntico al que
            # tenemos en nuestro servidor. Si es invalido devolvemos un status
            # code 403. Caso contrario devolvemos un status code 200 con el
            # valor hub.challenge
            if not valueHubToken == envFacebook['TOKEN']:
                return "Verification token mismatch", 403

            return valueHubChallenge, 200

    def sendMessage(self):
        # Obteniendo la data enviada en el body en formatoJSON
        data = request.get_json()

        recipientID = data['recipientID']
        messageText = data['messageText']
        data = {
            "recipient": {"id": recipientID},
            "message": {"text": messageText}
        }

        # Mostrar el efecto de escribiendo hacia el facebook
        self.sendAction(recipientID, 'typing_on')

        # Enviando el mensaje hacia facebook
        return self.post('facebook', 'me/messages', data)

    def receivedMessage(self):
        data = request.get_json()

        # Validar Type Message
        if data["object"] == "page":
            for entry in data["entry"]:
                for messagingEvent in entry["messaging"]:
                    senderID = messagingEvent["sender"]["id"]

                    self.log(messagingEvent)

                    # Alguien envía un mensaje
                    if messagingEvent.get("message"):
                        # Marcar como leído
                        self.sendAction(senderID, 'mark_seen')

                        message_text = messagingEvent["message"]["text"]

                        if message_text == 'welcome':
                            # Llamar al método de listar Países
                            # Mandar la data y mostrar en una botonera

                            pass

                        self.log('El usuario nos a mandado un mensaje')

                    # Confirmacion de delivery
                    if messagingEvent.get("delivery"):
                        # Desactivar el efecto de escribiendo
                        self.sendAction(senderID, 'typing_off')

                        self.log('Confirmacion de delivery')

                    # Confirmacion de option
                    if messagingEvent.get("option"):
                        self.log('Confirmacion de Option')

                    # Evento cuando usuario hace click en botones
                    if messagingEvent.get("postback"):
                        self.log('Evento cuando usuario hace click en botones')

        return 'Hemos recibido la data', 200

    def sendAction(self, recipientID, senderAction):
        # senderAction solo soporte tres formas
        # mark_seen  : Para marcar como leído el ultimo mensaje
        # typing_on  : Para mostrar el efecto de escribir
        # typing_off : Para retirar el efecto de escribir
        data = {
            "recipient": {
                "id": recipientID
            },
            "sender_action": senderAction
        }

        self.post('facebook', 'me/messages', data)

    def sendPostbackButton(self, data):
        # Obteniendo la data enviada en el body en formatoJSON

        templateData = postbackButton(data)
        self.log(templateData)

        return self.post('facebook', 'me/messages', templateData)

    def sendQuickReplies(self):
        # Obteniendo la data enviada en el body en formatoJSON
        data = request.get_json()

        recipientID = data['recipientID']
        data = {
            "recipient": {"id": recipientID},
            "messaging_type": "RESPONSE",
            "message": {
                "text": "Selecciona un Equipo",
                "quick_replies": [
                    {
                        "content_type": "text",
                        "title": "Alianza Lima",
                        "payload": "alianzaLima_QR",
                        "image_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Alianza_Lima.svg/1200px-Alianza_Lima.svg.png"
                    },{
                        "content_type": "text",
                        "title": "Universitario de Deportes",
                        "payload": "universitario_QR",
                        "image_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Alianza_Lima.svg/1200px-Alianza_Lima.svg.png"
                    }
                ]
            }
        }

        return self.post('facebook', 'me/messages', data)


    def sendConfigProfile(self):
        data = {
            "get_started":{
                "payload":"listCountries"
            },
            "greeting":[{
                "locale":"default",
                "text":"Hola {{user_first_name}}, te puedo ayudar en temas de Futbol!!"
            }],
            "persistent_menu": [
                {
                    "locale": "default",
                    "composer_input_disabled": False,
                    "call_to_actions": [
                        {
                            "type": "postback",
                            "title": "hablar con un agente",
                            "payload": "CARE_HELP"
                        },
                        {
                            "type": "postback",
                            "title": "sugerencias",
                            "payload": "CURATION"
                        },
                        {
                            "type": "web_url",
                            "title": "ver tienda",
                            "url": "https://www.originalcoastclothing.com/",
                            "webview_height_ratio": "full"
                        }
                    ]
                }
            ],
            "ice_breakers": [
                {
                    "question": "Listar Paises",
                    "payload": "listCountries",
                },
                {
                    "question": "Buscar Pais",
                    "payload": "searchCountries",
                }
            ]
            
        }
        # Enviando el mensaje hacia facebook
        return self.post('facebook', 'me/messenger_profile', data)




    def log(self, data):
        LogInfo('Clase FacebookController', data)

