from flask import Blueprint, request

from controllers.footballController import FootballController
football=FootballController()

footballBP = Blueprint('footballBP',__name__)

@footballBP.route('/list-countries')
def listCountries():
    return football.listCountries()