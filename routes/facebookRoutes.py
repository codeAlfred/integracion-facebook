from flask import Blueprint, request

from controllers.facebookController import FacebookController
facebook = FacebookController()

# registrando en el BluePrint
facebookBP = Blueprint('facebookBP', __name__)

@facebookBP.route('/validate-webhook')
def validateWebhook():
    return facebook.validate()

@facebookBP.route('/validate-webhook', methods=['POST'])
def receivedMessage():
    return facebook.receivedMessage()

@facebookBP.route('/send-message')
def sendMessage():
    return facebook.sendMessage()

@facebookBP.route('/sendAction', methods=['POST'])
def sendAction():
    return facebook.sendAction()

# @facebookBP.route('/send-postback-button')
# def sendPostbackButton():
#     data={
#         "recipientID": "2899626573462185",
#         "buttons":[
#             {
#                 "type":"postback",
#                 "title":"probando boton",
#                 "payload":"probando"
#             },
#             {
#                 "type":"postback",
#                 "title":"probando boton23",
#                 "payload":"probando23"
#             }
#         ]
#     }
#     return facebook.sendPostbackButton(data)

@facebookBP.route('/simular-list-paises')
def sendPostbackButton():
    data=request.get_json()
    return facebook.sendPostbackButton(data)

@facebookBP.route('/send-quick-replies')
def sendQuickReplies():
    return facebook.sendQuickReplies()

@facebookBP.route('/config-profile')
def sendConfigProfile():
    return facebook.sendConfigProfile()








################################################

# from controllers.FacebookConfigController import FacebookConfigController
# facebookConfig = FacebookConfigController()

# # Registrando en Blueprint
# facebookConfigBP = Blueprint('facebookConfigBP', __name__)

# # Crear propiedades
# @facebookConfigBP.route('/properties', methods=['POST'])
# def createProperties():
#     return facebookConfig.createProperties()

# # Eliminar Propiedades
# @facebookConfigBP.route('/properties', methods=['DELETE'])
# def deleteProperties():
#     return facebookConfig.deleteProperties()
