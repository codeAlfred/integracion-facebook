from flask import Flask
from utils.environment import Environment


def createAPP():
    app = Flask(__name__)

    # routes de Facebook
    from routes.facebookRoutes import facebookBP
    app.register_blueprint(facebookBP, url_prefix='/facebook')
    
    # routes de football
    from routes.footballRoutes import footballBP
    app.register_blueprint(footballBP, url_prefix='/football')

    return app



if __name__ == '__main__':
    # instanciar la clase de environment
    env=Environment()
    # obtener las variables de entornos para el proyecto
    config=env.general()

    app=createAPP()
    app.run(port=config['PORT'],  debug=config['DEBUG'])

